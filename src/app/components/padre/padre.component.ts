import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  mensajePadre1!:string;
  mensajePadre2!:string;
  mensajePadre3!:string;
  mensajePadre4!:string;
  mensajePadre5!:string;

  constructor() { }

  recibirMensaje1($event: string):void{
    this.mensajePadre1 = $event;
    console.log(this.mensajePadre1);
  }

  recibirMensaje2($event: string):void{
    this.mensajePadre2 = $event;
    console.log(this.mensajePadre2);
  }

  recibirMensaje3($event: string):void{
    this.mensajePadre3 = $event;
    console.log(this.mensajePadre3);
  }

  recibirMensaje4($event: string):void{
    this.mensajePadre4 = $event;
    console.log(this.mensajePadre4);
  }

  recibirMensaje5($event: string):void{
    this.mensajePadre5 = $event;
    console.log(this.mensajePadre5);
  }

  ngOnInit(): void {
  }

}
