import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  mensaje1 = 'los pollitos dicen';
  mensaje2 = 'pio pio pio';
  mensaje3 = 'cuando tienen';
  mensaje4 = 'hambre';
  mensaje5 = 'la mama gallina';

  @Output() EventoMensaje1 = new EventEmitter<string>();
  @Output() EventoMensaje2 = new EventEmitter<string>();
  @Output() EventoMensaje3 = new EventEmitter<string>();
  @Output() EventoMensaje4 = new EventEmitter<string>();
  @Output() EventoMensaje5 = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.EventoMensaje1.emit(this.mensaje1);
    this.EventoMensaje2.emit(this.mensaje2);
    this.EventoMensaje3.emit(this.mensaje3);
    this.EventoMensaje4.emit(this.mensaje4);
    this.EventoMensaje5.emit(this.mensaje5);
    
  }

}
